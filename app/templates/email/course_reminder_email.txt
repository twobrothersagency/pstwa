
Dear {{ user.firstName }},

This is a remainder that your {{ course.title }}, will start from {{ course.date }} from {{ course.startTime }}-{{ course.endTime }}
Click on the link to Login and View your course.{{{{config.CURRENT_SITE_URL}}}}/{{url_for('course',course_id=course.id)}}
Before Course start you must complete this course's quiz. {{config.CURRENT_SITE_URL}}/{{url_for('course_quiz',course_id=course.id)}}
Course: {{ course.title }}
Venue: {{ course.venue }}
Description: {{ course.description }}

Many thanks,
The Pulsestart Team