import time, sys, json
import datetime

from sqlalchemy import func
from app import app, db, celery
from flask import render_template
from rq import get_current_job
from app.models import Task, User, Post, Course, CourseAttendees,QuizResult
from app.email import send_email
from app import app


def example(seconds):
    job = get_current_job()
    print('Starting task')
    for i in range(seconds):
        job.meta['progress'] = 100.0 * i / seconds
        job.save_meta()
        print(i)
        time.sleep(1)
    job.meta['progress'] = 100
    job.save_meta()
    print('Task completed')

def _set_task_progress(progress):
    job = get_current_job()
    if job:
        job.meta['progress'] = progress
        job.save_meta()
        task = Task.query.get(job.get_id())
        task.user.add_notification('task_progress', {'task_id': job.get_id(),
                                                     'progress': progress})
        if progress >= 100:
            task.complete = True
        db.session.commit()


@celery.task()
def send_email_to_attendees(course_id, subject, body):
    """
    Send email to all Attendees by an Admin
    """
    user_qs = db.session.query(CourseAttendees.user_id).filter(CourseAttendees.course_id == int(course_id)).\
        distinct().all()
    user_ids = [user.user_id for user in user_qs]
    user_email_qs = db.session.query(User.email).filter(User.id.in_(user_ids)).all()
    user_emails = [user.email for user in user_email_qs]
    if user_emails:
        for email in user_emails:
            send_email(subject,
                       sender='backupall37@gmail.com',
                       recipients=[email],
                       text_body=body,
                       html_body=None)

@celery.task()
def course_reminder_email():
    """
    send email to all users, configured days prior to an enrolled course
    days should be configured in `REMIND_ENROLLED_COURSE` config
    """
    current_date = datetime.datetime.now()
    with app.app_context():
        remind_prior_days = app.config['REMIND_COURSE_PRIOR_DAYS']
    
    after_some_days = current_date + datetime.timedelta(days=remind_prior_days)
    course_with_users = db.session.query(Course,User).select_from(CourseAttendees).\
                        join(Course,CourseAttendees.course_id == Course.id).\
                        join(User,User.id==CourseAttendees.user_id).\
                        filter(func.Date(Course.date) == after_some_days.date()).all()
    for reminder in course_with_users:
        course = reminder[0]
        user = reminder[1]
        send_email('[Pulsestart Web Application] Course Start Reminder',
                sender=app.config['ADMINS'][0], recipients=[user.email],
                text_body=render_template('email/course_reminder_email.txt',course=course, user=user),
                html_body=render_template('email/course_reminder_email.html', course=course, user=user),
        )


@celery.task()
def final_course_reminder_email():
    """
    **This is a final reminder**

    send email to all users, configured hours prior to an enrolled course
    days should be configured in `FINAL_REMIND_PRIOR_HOURS` variable
    """
    current_date = datetime.datetime.now()
    
    with app.app_context():
        remind_before_hours = app.config['FINAL_REMIND_PRIOR_HOURS']
    
    #dropping seconds and milliseconds for absolute hour-minute comparision
    after_some_hours_start = (
                                current_date + datetime.timedelta(hours=24)
                             ).replace(microsecond=0,second=0)

    after_some_hours_end = (
                            after_some_hours_start+\
                            datetime.timedelta(minutes=(59 - after_some_hours_start.minute))
                            ).replace(microsecond=0,second=0)

    
    course_with_users = db.session.query(Course,User).select_from(CourseAttendees).\
                        join(Course,CourseAttendees.course_id == Course.id).\
                        join(User,User.id==CourseAttendees.user_id).\
                        filter(func.Date(Course.date) == after_some_hours_start.date()).all()

    
    for reminder in course_with_users:
        course = reminder[0]
        user = reminder[1]
        #this will only tigger if the calculated prior hours lies between the start time hourly range.
        if course.startTime >= after_some_hours_start.time() and course.startTime <= after_some_hours_end.time():
            print('sending')
            send_email('[Pulsestart Web Application] Course Start Final Reminder',
                    sender=app.config['ADMINS'][0], recipients=[user.email],
                    text_body=render_template('email/course_reminder_email.txt',course=course, user=user),
                    html_body=render_template('email/course_reminder_email.html', course=course, user=user),
            )

@celery.task()
def send_course_quiz_remainder():
    """ 
        send email to  students enrolled in course 
        who haven't completed quiz 48 hrs before course start.
    """
    current_date = datetime.datetime.now()
    after_48_hours = current_date + datetime.timedelta(hours=48)

    course_attendees = db.session.query(User,Course).\
                        select_from(CourseAttendees).\
                        join(Course,Course.id==CourseAttendees.course_id).\
                        join(User,CourseAttendees.user_id==User.id).\
                        filter(func.Date(Course.date) == after_48_hours.date()).all()
    
    for attendee in course_attendees:
        user = attendee[0]
        course = attendee[1]
        #if the attendee hasn't taken the quiz
        if QuizResult.query.filter(QuizResult.user_id!=user.id).first():
            send_email('[Pulsestart Web Application] Quiz Reminder',
                    sender=app.config['ADMINS'][0], recipients=[user.email],
                    text_body=render_template('email/quiz_reminder_email.txt',course=course, user=user),
                    html_body=render_template('email/quiz_reminder_email.html', course=course, user=user),
                )

@celery.task()
def send_instructor_quiz_incomplete_notice():
    """
        Send instructor and Suzie an email with list of Users 
        who have not completed Quiz on day of course.
    
    """
    current_date = datetime.datetime.now()

    course_attendees = db.session.query(User.id).\
                    select_from(CourseAttendees).\
                    join(Course,Course.id==CourseAttendees.course_id).\
                    join(User,CourseAttendees.user_id==User.id).\
                    filter(func.Date(Course.date) == current_date.date()).all()

    user_info = []
    for attendee in course_attendees:
        user = attendee[0]
        course = attendee[1]
        #if the attendee hasn't taken the quiz
        if QuizResult.query.filter(QuizResult.user_id!=user.id).first():
            user_info.append({'name':user.firstName+' '+user.lastName,'email':user.email,'course':course})
    
    if user_info:
        send_email('[Pulsestart Web Application] Quiz Reminder',
                sender=app.config['ADMINS'][0], recipients=[app.config['ADMINS'][0]],
                text_body=render_template('email/instructor_quiz_reminder.txt',users=user_info),
            html_body=render_template('email/instructor_quiz_reminder.html', users=user_info)
        )